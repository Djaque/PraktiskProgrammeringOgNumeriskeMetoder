#include <stdio.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
#include <assert.h>

double rosenbrock(const gsl_vector *point, void *params){
	double x, y;
	x = gsl_vector_get(point, 0);
	y = gsl_vector_get(point, 1);

	return (1 - x) * (1 - x) + 100 * (y - x * x) * (y - x * x);
}

double derivative(const gsl_vector *point, void *params, gsl_vector *f){
	double x = gsl_vector_get(point, 0);
	double y = gsl_vector_get(point, 1);
	double dx = 2*(200*x*x*x-200*x*y+x-1);	//derivative of f with respect to x
	double dy = 200*(y-x*x);	//derivative of f with respect to y

	gsl_vector_set(f, 0, dx);
	gsl_vector_set(f, 1, dy);
}

double combined(const gsl_vector *point, void *params, double *f, gsl_vector *df){
	*f = rosenbrock(point, params);
	derivative(point, params, df);
}

int main(int argc, char const *argv[])
{
	int counter;
	int status;

	const gsl_multimin_fdfminimizer_type *T;
	gsl_multimin_fdfminimizer *s;

	gsl_multimin_function_fdf the_function;

	the_function.n = 2;
	the_function.f = &rosenbrock;
	the_function.df = &derivative;
	the_function.fdf = &combined;
	the_function.params = NULL;

	gsl_vector *start_point = gsl_vector_alloc(2);
	gsl_vector_set(start_point, 0, -1);
	gsl_vector_set(start_point, 1, -2);

	T = gsl_multimin_fdfminimizer_conjugate_fr;
	s = gsl_multimin_fdfminimizer_alloc(T, 2);

	double step = 0.01;
	double tol = 1e-10;

	gsl_multimin_fdfminimizer_set(s, &the_function, start_point, step, tol);

	do{
		counter++;
		status = gsl_multimin_fdfminimizer_iterate(s);

		if (status)
			break;
		status = gsl_multimin_test_gradient((*s).gradient, 1e-3);

		if (status == GSL_SUCCESS){
			printf("Converged at\n");
		}
		double x = gsl_vector_get((*s).x, 0);
		double y = gsl_vector_get((*s).x, 1);

		printf("%5d %.5f %.5f %10.5f\n", counter,
			   x,
			   y,
			   (*s).f);

	} while (status == GSL_CONTINUE && counter < 1000);

	gsl_multimin_fdfminimizer_free(s);
	gsl_vector_free(start_point);



	return 0;
}