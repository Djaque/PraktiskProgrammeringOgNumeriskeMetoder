struct rparams{
    double a;
    double b;
};

int rosenbrock_f (const gsl_vector * x, void *params, gsl_vector * f);
