#include <stdio.h>

int is_equal(double a, double b, double tau, double epsilon);

int main(int argc, char const *argv[])
{
	printf("\n---Question 3---\n");
	printf("Write an 'is-equal-function'. \n");

	double a = 1.1;
	double b = 1.05;
	double tau = 0.06;
	double epsilon = 1e-6;
	printf("checking if a = %g and b = %g with tau = %g:\n", a, b, tau);
	if (is_equal(a,b,tau,epsilon) == 1){
		printf("True!\n");
	}
	else
		printf("False!\n");

	tau = 0.01;
	printf("checking if a = %g and b = %g with tau = %g:\n", a, b, tau);
	if (is_equal(a,b,tau,epsilon) == 1){
		printf("True!\n");
	}
	else
		printf("False!\n");
	return 0;
}