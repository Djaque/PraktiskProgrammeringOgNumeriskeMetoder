#include "stdio.h"
#include "limits.h"
#include "math.h"
#include "float.h"


int main(int argc, char const *argv[])
{
	printf("---Question 1---\n");
	printf("i) Find maximum representable integer: %i.\n",INT_MAX);
	int i = 1;
	
	while (i+1>i){i++;}
	printf("Max integer using while-loop:	%i\n",i);

	for (int j=1;j<j+1;j++){i = j;}
	printf("Max integer using for-loop: 	%i\n",i+1);	//Need +1 to do the last for-loop

	i=0;
	do {i++;}
	while(i<i+1);
	printf("Max integer using do-while-loop:%i\n",i);



	printf(" \n");
	printf("ii) Find minimum representable integer: %i.\n",INT_MIN);
	i = 0;
	
	while (i-1<i){i--;}
	printf("Min integer using while-loop:	%i\n",i);

	for (int i=0;i<i-1;i--){i--;}
	printf("Max integer using for-loop: 	%i\n",i);

	i=0;
	do {i--;}
	while(i>i-1);
	printf("Max integer using do-while-loop:%i\n",i);



	printf(" \n");
	printf("iii) The Machine Epsilon.\n");
	printf("- With while-loops:\n");
	float x = 1;
	while(1+x!=1){x /= 2;}
	printf("Using a float: 		x=%g	(true value: %g)\n", x*2,FLT_EPSILON);

	double y = 1.0;
	while(1+y!=1){y /= 2;}
	printf("Using a double: 	y=%g	(true value: %g)\n", y*2,DBL_EPSILON);

	long double z = 1.0;
	while(1+z!=1){z /= 2;}
	printf("Using a long double: 	z=%Lg	(true value: %Lg)\n", z*2,LDBL_EPSILON);


	printf(" \n");
	printf("- With for-loops:\n");
	x = 1;
	for(int i;x+1!=1;i--){x/=2;}
	printf("Using a float: 		x=%g	(true value: %g)\n", x*2,FLT_EPSILON);

	y = 1;
	for(int i;y+1!=1;i--){y/=2;}
	printf("Using a double: 	y=%g	(true value: %g)\n", y*2,DBL_EPSILON);

	z = 1;
	for(int i;z+1!=1;i--){z/=2;}
	printf("Using a long double: 	z=%Lg	(true value: %Lg)\n", z*2,LDBL_EPSILON);

	
	printf(" \n");
	printf("- With do-while-loops:\n");

	x = 1;
	do{x/=2;}
	while(x+1!=1);
	printf("Using a float: 		x=%g	(true value: %g)\n", x*2,FLT_EPSILON);

	y = 1;
	do{y/=2;}
	while(y+1!=1);
	printf("Using a double: 	y=%g	(true value: %g)\n", y*2,DBL_EPSILON);


	z=1;
	do{z/=2;}
	while(z+1!=1);
	printf("Using a long double: 	z=%Lg	(true value: %Lg)\n", z*2,LDBL_EPSILON);


	printf("\n---Question 2---\n");
	int max = INT_MAX/2;
	printf("i) Calculate the two sums.\n");
	float sum_up_float = 0;
	float sum_down_float = 0;
	
	for(int i=1;i<=max;i++){
		sum_up_float = sum_up_float + 1.0f/i;
	}
	for(int i=0;i<=max;i++){
		if(i==max){
			sum_down_float += 1.0f;
		}
		else{
			sum_down_float = sum_down_float + 1.0f/(max-i);
		}
	}
	printf("sum_up = %g\n", sum_up_float);
	printf("sum_down = %g\n", sum_down_float);

	printf("\nii) Explain the difference.\n");
	printf("The difference is due to float precision.\n");

	printf("\niii) Does it converge as a function of max?.\n");
	printf("No.\n");
	
	printf("\niv) Do the same calculation using doubles.\n");	
	double sum_up_double = 0.0;
	double sum_down_double = 0.0;
	for(int i=1;i<=max;i++){
		sum_up_double = sum_up_double + 1.0/i;
	}
	for(int i=0;i<=max;i++){
		if(i==max){
			sum_down_double += 1.0;
		}
		else{
			sum_down_double = sum_down_double + 1.0/(max-i);
		}
	}
	printf("sum_up = %g\n", sum_up_double);
	printf("sum_down = %g\n", sum_down_double);

	return 0;
}