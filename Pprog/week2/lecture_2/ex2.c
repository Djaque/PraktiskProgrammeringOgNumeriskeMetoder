#include"stdio.h"
#include"math.h"
#include"complex.h"

int main(){
    printf("1) Calculate gamma(5).\n");
    double gam = tgamma(5.0);
    printf("Answer: %g\n",gam);
    printf("\n");
    
    printf("2) Calculate J1(0.5).\n");
    double j = j1(0.5);
    printf("Answer: %g\n",j);
    printf("\n");

    printf("3) Calculate sqrt(-2).\n");
    complex z = csqrt(-2.0);
    printf("Answer: %g + i%g.\n",creal(z),cimag(z));
    printf("\n");

    printf("4) Calculate e^i.\n");
    complex e = cexp(I);
    printf("Answer: %g + i%g.\n",creal(e),cimag(e));
    printf("\n");

    printf("5) Calculate e^(i*pi).\n");
    complex p = cexp(I*M_PI);
    printf("Answer: %g + i%g.\n",creal(p),cimag(p));
    printf("\n");

    printf("6) Calculate i^e.\n");
    complex k = cpow(I,M_E);
    printf("Answer: %g + i%g.\n",creal(k),cimag(k));
    printf("\n");

    printf("--Part II--.\n");
    printf("Significant digits.\n");
    float a = 0.111111111111111111111111111;
    printf("Float:          %.25g\n",a);
    double b = 0.111111111111111111111111111;
    printf("Double:         %.25lg\n",b);
    long double c = 0.111111111111111111111111111L;
    printf("Long double:    %.25Lg\n",c);



    return 0;
}