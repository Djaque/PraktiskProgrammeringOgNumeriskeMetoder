#include <stdio.h>
#include "nvector.h"
#include <stdlib.h>

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value){ 
	(*v).data[i]=value; 
}

double nvector_get(nvector* v, int i){return (*v).data[i]; }


void nvector_add(nvector* a, nvector* b){
	if (a->size == b->size){
		for (int n=0; n<(a->size); n++){
			double value = a->data[n]+b->data[n];
			nvector_set(a,n,value);
		}
	}
	else{
		printf("Exception: Vectors must be of same length.\n");
	}
}


void nvector_sub(nvector* a, nvector* b){
	if (a->size == b->size){
		for (int n=0; n<(a->size); n++){
			double value = a->data[n] - b->data[n];
			nvector_set(a,n,value);
		}
	}
	else{
		printf("Exception: Vectors must be of same length.\n");
	}
}


int nvector_equal( nvector* a, nvector* b){
	if (a->size == b->size){
		int continue_loop = 1;
		int n = 0;
		while (continue_loop && n<(a->size)){
			if (a->data[n]==b->data[n])
			{
				n++;
			}
			else continue_loop=0;
		}
	}
	else{
		printf("Exception: Vectors must be of same length.\n");
	}
}

void nvector_print(char *s, nvector* a){
	printf("\n%s\n", s);
	printf("[");
	for (int n=0; n<a->size-1; n++){
		printf("%g, ", nvector_get(a,n));
	}
	printf("%g]\n", nvector_get(a,a->size-1));
}


double nvector_dot_product(nvector* v, nvector* u){
	double s;
	if (v->size == u->size){
		for (int i=0; i<(v->size); i++){
			double value = nvector_get(v,i)*nvector_get(u,i);
			s += value;
		}
	}
	else{
		printf("Exception: Vectors must be of same length.\n");
	}
	return s;
}


void nvector_set_zero(nvector* a){
	for (int n=0; n<(a->size); n++){
			nvector_set(a,n,0);
		}
}


void nvector_scale(nvector* a, double x){
	for (int n=0; n<(a->size); n++){
			nvector_set(a,n,a->data[n]*x);
		}
}