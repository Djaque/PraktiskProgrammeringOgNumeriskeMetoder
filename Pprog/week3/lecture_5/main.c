#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 6;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);

	double vi = nvector_get(v, i);
	if (vi == value) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_print("a = ",a);
	nvector_print("b = ",b);
	
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_dot ...\n");
	
	nvector_print("a = ",a);
	nvector_print("b = ",b);

	double s = nvector_dot_product(a, b);
	printf("Dotproduct is = %g\n", s);


	printf("\nmain: testing nvector_set_zero ...\n");
	nvector_set_zero(a);
	nvector_print("a = ",a);
	vi = nvector_get(a, i);
	if (vi == 0) printf("test passed\n");
	else printf("test failed\n");

	
	printf("\nmain: testing nvector_sub ...\n");
	vi = nvector_get(a,i)-nvector_get(b,i);
	nvector_sub(a,b);
	nvector_print("a = ",a);
	if (vi == nvector_get(a,i)) printf("test passed\n");
	else printf("test failed\n");	

	printf("\nmain: testing nvector_scale ...\n");
	vi = nvector_get(a,i)*3;
	nvector_scale(a,3);
	nvector_print("a = ",a);
	if (vi == nvector_get(a,i)) printf("test passed\n");
	else printf("test failed\n");


	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);

	return 0;
}