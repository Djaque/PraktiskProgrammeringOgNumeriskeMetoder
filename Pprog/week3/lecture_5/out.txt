
main: testing nvector_alloc ...
test passed

main: testing nvector_set and nvector_get ...
test passed

main: testing nvector_add ...

a = 
[0.394383, 0.79844, 0.197551, 0.76823, 0.55397, 0.628871]

b = 
[0.783099, 0.911647, 0.335223, 0.277775, 0.477397, 0.364784]

a+b should   = 
[1.17748, 1.71009, 0.532774, 1.046, 1.03137, 0.993655]

a+b actually = 
[1.17748, 1.71009, 0.532774, 1.046, 1.03137, 0.993655]
test passed

main: testing nvector_dot ...

a = 
[1.17748, 1.71009, 0.532774, 1.046, 1.03137, 0.993655]

b = 
[0.783099, 0.911647, 0.335223, 0.277775, 0.477397, 0.364784]
Dotproduct is = 3.80508

main: testing nvector_set_zero ...

a = 
[0, 0, 0, 0, 0, 0]
test passed

main: testing nvector_sub ...

a = 
[-0.783099, -0.911647, -0.335223, -0.277775, -0.477397, -0.364784]
test passed

main: testing nvector_scale ...

a = 
[-2.3493, -2.73494, -1.00567, -0.833324, -1.43219, -1.09435]
test passed
