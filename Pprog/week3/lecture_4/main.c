#include "komplex.h"
#include "stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	komplex_print("a =",a);
	komplex_print("b =",b);
	
	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a+b should be	= ", R);
	komplex_print("a+b actually	= ", r);

/* the following is optional */

	if( komplex_equal(R,r) )
		printf("test 'add' passed :) \n");
	else
		printf("test 'add' failed: debug me, please... \n");

	printf(" \n");
	printf("testing komplex_sub...\n");
	r = komplex_sub(a,b);
	komplex_set(&R,-2,-2);
	komplex_print("a-b should be	= ", R);
	komplex_print("a-b actually	= ", r);

	if( komplex_equal(R,r) )
		printf("test 'sub' passed :) \n");
	else
		printf("test 'sub' failed: debug me, please... \n");

	printf(" \n");
	printf("testing komplex_mul...\n");
	r = komplex_mul(a,b);
	komplex_set(&R,-5,10);
	komplex_print("a*b should be	= ", R);
	komplex_print("a*b actually	= ", r);

	if( komplex_equal(R,r) )
		printf("test 'mul' passed :) \n");
	else
		printf("test 'mul' failed: debug me, please... \n");




	return 0;
}