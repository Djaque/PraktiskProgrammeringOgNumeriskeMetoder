#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <math.h>

int sine_ode(double x, const double y[], double dydx[], void* params){
	dydx[0] = y[1];
	dydx[1] = -y[0];

	return GSL_SUCCESS;
}


double sine_func(double x){
	gsl_odeiv2_system sys;
	sys.function = sine_ode;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = NULL;

	double y[2] = {0, 1.0};

	double x_end = fmod(x,2*M_PI);	//Using periodicity of the sine function

	double hstart = copysign(1e-3,x);
	gsl_odeiv2_driver * driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, hstart, 1e-6, 1e-6);
	
	double t = 0.0;
	int status = gsl_odeiv2_driver_apply(driver, &t, x_end, y);
	
	gsl_odeiv2_driver_free(driver);
	return y[0];
}