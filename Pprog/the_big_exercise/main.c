#include <stdio.h>
#include "sine.h"
#include <math.h>

int main(int argc, char const *argv[])
{
	printf("X \t Y \t expected Y\n");
	for (double x = -0.5; x < 10; x+=0.2)
	{
		printf("%g \t %g\t %g\n", x, sine_func(x), sin(x));
	}

	return 0;
}