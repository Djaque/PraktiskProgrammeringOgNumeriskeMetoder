#include <stdio.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <math.h>

double norm_integral (double x, void * params){
	double alpha = *(double *) params;
  double result = exp(-alpha*x*x);
	
  return result;
}

double hamilton_integral (double x, void * params){
	double alpha = *(double *) params;
  double result = (-alpha*alpha*x*x + alpha + x*x)*exp(-alpha*x*x)/2;

	return result;
}

double integral(double alpha, double (*function)(double x, void *params)){
  gsl_function F;

  F.function = function;
  F.params = (void *)&alpha;
  double a = 0;
  double size = 1000;
  double acc = 1e-7, eps = 1e-7;
  double result, error;
  gsl_integration_workspace *w = gsl_integration_workspace_alloc(size);
  int status = gsl_integration_qagiu(&F, a, acc, eps, size, w, &result, &error);

  gsl_integration_workspace_free(w);

  if(status!=GSL_SUCCESS) return NAN;
  else return 2*result;
}

double expectation_energy(double alpha){
  double result = integral(alpha, &hamilton_integral)/integral(alpha, &norm_integral);
  return result;
}

int main(int argc, char const *argv[])
{
  printf("Alpha \t Energy\n");
    for (double alpha = 0.2; alpha<9.0; alpha+=0.01){
      printf("%g \t %g\n", alpha, expectation_energy(alpha));
    }
  return 0;
}