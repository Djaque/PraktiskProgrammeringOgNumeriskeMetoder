#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <getopt.h>

int diff_eq(double x, const double y[], double y_prime[], void *params){
	double epsilon = *(double*) params;
	y_prime[0] = y[1];
	y_prime[1] = 1-y[0]+epsilon*y[0]*y[0];
	return GSL_SUCCESS;
}

int main(int argc, char *argv[]){
	
	double epsilon = 0, uprime = 0;
	while (1){
	struct option long_options[] =
	{
		{"epsilon", required_argument, NULL, 'e'},
		{"uprime" , required_argument, NULL, 'p'},
		{0, 0, 0, 0}
	};
	int opt = getopt_long (argc, argv, "e:p:", long_options, NULL);
	if( opt == -1 ) break;
	switch (opt) {
		case 'e': epsilon = atof (optarg); break;
		case 'p': uprime  = atof (optarg); break;
		default:
			fprintf (stderr, "Usage: %s --epsilon epsilon --uprime uprime\n", argv[0]);
			exit (EXIT_FAILURE);
		}
	}
/* end of processing command-line options */

	gsl_odeiv2_system orbit;
	orbit.function = diff_eq;
	orbit.jacobian = NULL;
	orbit.dimension = 2;
	orbit.params = (void *) &epsilon;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
	double phi_max = 30 * M_PI, delta_phi = 0.05;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
			(&orbit, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double t = 0, y[2] = { 1, uprime };
	for (double phi = 0; phi < phi_max; phi += delta_phi) {
		int status = gsl_odeiv2_driver_apply (driver, &t, phi, y);

		printf ("%g \t %g\n", phi, y[0]);
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
		}

	gsl_odeiv2_driver_free (driver);
	return 0;
}