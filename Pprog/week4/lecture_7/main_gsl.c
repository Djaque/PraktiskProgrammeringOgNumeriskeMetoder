#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_sf_airy.h>

int main()
{
	int size = 800;
	gsl_vector * x_values = gsl_vector_alloc(size);
	gsl_vector * airy_A = gsl_vector_alloc(size);
	gsl_vector * airy_B = gsl_vector_alloc(size);

	double x = -15.0;
	for (int i = 0; i<size; i++){
		gsl_vector_set(x_values,i,x);
		x+=0.02;
	}

	for (int i = 0; i<size; i++){
		double x_val = gsl_vector_get(x_values,i);
		double A_val = gsl_sf_airy_Ai(x_val,GSL_PREC_SINGLE);
		double B_val = gsl_sf_airy_Bi(x_val,GSL_PREC_SINGLE);
		gsl_vector_set(airy_A,i,A_val);
		gsl_vector_set(airy_B,i,B_val);

		printf("%g \t %g \t %g \n", x_val, A_val, B_val);
	}


	gsl_vector_free(x_values);
	gsl_vector_free(airy_A);
	gsl_vector_free(airy_B);

	return 0;
}