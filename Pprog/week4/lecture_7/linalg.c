#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>
#include <assert.h>


int main(int argc, char const *argv[])
{
	gsl_matrix * A = gsl_matrix_calloc(3,3);	//Initialize matrix A with 0 in all entrences
	gsl_matrix * AA = gsl_matrix_calloc(3,3);	// For copying A to AA, as HH destroys A upon execution
	gsl_vector * b = gsl_vector_calloc(3);
	gsl_vector * x = gsl_vector_calloc(3);
	gsl_vector * y = gsl_vector_calloc(3);

	//Initialize matrix A with given values
	gsl_matrix_set(A,0,0,6.13); gsl_matrix_set(A,0,1,-2.90); gsl_matrix_set(A,0,2,5.86);
	gsl_matrix_set(A,1,0,8.08); gsl_matrix_set(A,1,1,-6.31); gsl_matrix_set(A,1,2,-3.89);
	gsl_matrix_set(A,2,0,-4.36); gsl_matrix_set(A,2,1,1.00); gsl_matrix_set(A,2,2,0.19);

	//Initialize vector b with given values
	gsl_vector_set(b,0,6.23);
	gsl_vector_set(b,1,5.37);
	gsl_vector_set(b,2,2.29);


	//Solve linear equation system
	gsl_matrix_memcpy(AA,A);	// Copy A to AA
	gsl_linalg_HH_solve(AA,b,x);

	printf("Linear system of equations.\n");
	printf("Solution is:\n");
	printf("x = [%g %g %g]\n", gsl_vector_get(x,0), gsl_vector_get(x,1), gsl_vector_get(x,2));
	
	printf("Checking solution with assert...\n");
	gsl_blas_dgemv(CblasNoTrans, 1, A, x, 0.0, y);
	
	double epsilon = 1e-10;
	for (int i = 0; i < 3; ++i)
	{
		assert(fabs(gsl_vector_get(b,i)-gsl_vector_get(y,i))<epsilon);
	}
	
	printf("Success!!\n");

	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(y);

	return 0;
}