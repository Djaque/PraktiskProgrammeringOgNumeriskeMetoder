#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <getopt.h>

int diff_equation(double x, const double y[], double y_prime[], void *params){
	y_prime[0] = y[1];
	y_prime[0] = 2/sqrt(M_PI) * exp(-x*x);
	return GSL_SUCCESS;
}


int main(int argc, char *argv[]){
	double a = 0, b = 0, dx = 0;

	while(1){
	int opt = getopt(argc, argv, "a:b:d:");
	if(opt == -1) break;
		switch (opt)
		{
		case 'a':
			a = atof(optarg);
			break;
		case 'b':
			b = atof(optarg);
			break;
		case 'd':
			dx = atof(optarg);
			break;
		default: 
			fprintf(stderr, "Usage: %s [-a lower bound] [-b upper bound] [-d dx~interval]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}

	gsl_odeiv2_system sys;

	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double hstart, epsabs = 1e-6, epsrel = 1e-6;
	hstart = copysign(1e-3, a);

	gsl_odeiv2_driver *driver;
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double y[1] = {0};
	double xi = 0;

	int status = gsl_odeiv2_driver_apply(driver, &xi, a, y);
	
	if (status != GSL_SUCCESS){
		fprintf(stderr, "Funtion: status = %i\n", status);
	}
	printf("\n\n");
	hstart = copysign(1e-3, b);
	
	driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	for (double x = xi; x < b; x += copysign(dx, b))
	{
		int status = gsl_odeiv2_driver_apply(driver, &xi, x, y);
		printf("%g %g %g \n", x, y[0], erf(x));
		if (status != GSL_SUCCESS)
		{
			fprintf(stderr, "Funtion: status = %i\n", status);
		}
	}
	
	gsl_odeiv2_driver_free(driver);

	return 0;
}