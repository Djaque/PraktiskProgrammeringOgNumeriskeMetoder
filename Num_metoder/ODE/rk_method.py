from math import *
import numpy as np


def rkstep12(F, t, y, h):
    k0 = F(t, y)
    k1 = F(t + np.ones(t.size)*h/2, y + k0 * (h/2))
    yh = y + k1 * h
    err = np.linalg.norm((k0 - k1) * (h/2))

    return yh, err


def rkdriver(F, tlist, ylist, b, step=1e-2, eps=1e-6, acc=1e-6):
    a = tlist[-1]
    nsteps = 0

    while nsteps < 999:
        t0 = tlist[-1]
        y0 = ylist[-1]
        if t0 >= b:
            break
        if t0 + step > b:
            step = b - t0
        (y, err) = rkstep12(F, t0, y0, step)
        tol = (acc + np.linalg.norm(y)*eps) * sqrt(step/(b-a))
        if err < tol:
            nsteps += 1
            tlist = np.append(tlist, (t0+step))
            ylist.append(y)
        if err == 0:
            step *= 2
        else:
            step *= pow(tol/err, 0.25) * 0.95

    return tlist, ylist
