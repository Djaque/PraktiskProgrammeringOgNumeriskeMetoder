from math import *
import numpy as np
import rk_method as ode
from matplotlib import pyplot as plt


def F(t, y):
    return np.array([y[1], -y[0]])


if __name__ == '__main__':
    a = 0
    b = 4 * pi
    eps = 0.01
    acc = 0.01
    hstart = 0.1
    x = np.array([a])
    y = []
    y.append(np.array([0, 1]))
    tlist, ylist = ode.rkdriver(F, x, y, b, hstart, eps, acc)

    print("# m=1, S=4")
    for i in range(len(tlist)):
        print(tlist[i], ylist[i][0])

    print("\n# m=2, S=6")
    for i in range(len(tlist)):
        print(tlist[i], ylist[i][1])

    plt.plot(tlist, ylist)
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("Sine and cosine function from ODE solution")
    plt.savefig("plot.svg")
