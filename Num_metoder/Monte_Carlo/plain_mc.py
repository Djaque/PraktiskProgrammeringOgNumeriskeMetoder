import numpy as np
from math import *


def random_x_in_region(a, b):
    assert a.size == b.size
    x = a + np.random.rand(1, len(a))*(b-a)
    assert x.size == a.size

    if x.size > 1:
        return x[0]
    else:
        return x


def plain_mc(func, a, b, N):
    assert a.size == a.size
    V = np.prod(b-a)
    sum1 = 0
    sum2 = 0

    for i in range(N):
        x = random_x_in_region(a, b)
        fx = func(x)
        sum1 += fx
        sum2 += fx**2

    avg = sum1/N
    var = sum2/N-avg**2

    result = avg*V
    error = sqrt(var/N)*V

    return result, error


