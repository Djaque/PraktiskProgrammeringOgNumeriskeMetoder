import numpy as np
from math import *
import plain_mc as mc
import sys
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
import time


def f1(x):
    return sqrt(x)


def f2(x):
    assert len(x) == 3, \
        "Length is %r" % (len(x))
    return 1/(pi**3*(1-cos(x[0])*sin(x[1])*cos(x[2])))


def fit_func(x, a):
    return a * 1/np.sqrt(x)


def main_A():
    t = time.time()
    a = np.array([0], dtype=np.float64)
    b = np.array([1], dtype=np.float64)
    N = int(2e6)

    Q, err = mc.plain_mc(f1, a, b, N)
    expected = 2 / 3

    print("\n Calculating the integral of f(x) = sqrt(x) from ", a, " to ", b, " using plain Monte Carlo.")
    print("Result       = ", Q)
    print("Expected     = ", expected)
    print("Error        = ", err)
    print("Time         =  %.3f" % (time.time() - t))

    tt = time.time()
    a = np.array([0, 0, 0], dtype=np.float64)
    b = np.array([pi, pi, pi], dtype=np.float64)
    N = int(1e6)

    Q, err = mc.plain_mc(f2, a, b, N)
    expected = gamma(1 / 4) ** 4 / (4 * pi ** 3)

    print("\n Calculating the integral of f(x) in 3 dimensions given in the exercise from ", a, " to ", b,
          " using plain Monte Carlo.")
    print("Result       = ", Q)
    print("Expected     = ", expected)
    print("Error        = ", err)
    print("Time         =  %.3f" % (time.time() - tt))

    print("\n Total time used = %.3f" % (time.time() - t))


def main_B():
    t = time.time()
    N = int(1e3)
    x = np.arange(5, N, 10)
    y = np.empty(x.size)
    counter = 0
    a = np.array([0])
    b = np.array([1])
    for i in range(x.size):
        Q, err = mc.plain_mc(f1, a, b, x[i])
        y[counter] = err
        counter += 1
    fit_params, pcov = curve_fit(fit_func, x, y)
    plt.scatter(x, y, label="Simulation data")
    fit_string = "fit = %.3f *1/sqrt(x)" % fit_params[0]
    plt.plot(x, fit_func(x, fit_params[0]), label=fit_string)
    plt.xlabel("N")
    plt.ylabel("Error")
    plt.legend()
    plt.savefig("plot.svg")
    print("Time = %.3f" % (time.time() - t))
    print("See plot for result.")


if __name__ == '__main__':
    if sys.argv[1:][0] == 'A':
        main_A()
    elif sys.argv[1:][0] == 'B':
        main_B()
    else:
        for arg in sys.argv[1:]:
            print("Please provide the correct assignment request. Provided: ", arg)
