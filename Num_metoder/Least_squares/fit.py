import sys
sys.path.append('../Linear_equations')
import QR
import numpy as np


def ls_fit(fs, x, y, dy):
    n = len(x)
    m = len(fs)
    A = np.empty([n, m])
    b = np.empty(n)
    for i in range(n):
        b[i] = y[i] / dy[i]
        for k in range(m):
            A[i, k] = fs[k](x[i]) / dy[i]
    (Q, R) = QR.qr_decomp(A)

    c = QR.qr_solve(Q, R, b)

    inverse_R = np.linalg.inv(R)
    S = np.matmul(inverse_R, np.transpose(inverse_R))

    return (c, S)