import numpy as np
from matplotlib import pyplot as plt
import fit
from math import *

def p0(z): return np.log(z)
def p1(z): return 1.0
def p2(z): return z


if __name__ == '__main__':
    funs = np.array([p0, p1, p2])
    x = np.array([0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9])
    y = np.array([-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75])
    dy = np.array([1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478])

    print(">>Exercise A")
    print("Fit the following data:")
    print("x = ", x)
    print("y = ", y)
    print("error_y = ", dy)
    print("\nRunning least squares fit...")
    c, S = fit.ls_fit(funs, x, y, dy)
    print("Done. Found the following coefficients:")
    print("c = ", c)

    xx = np.linspace(0.1, 11, 100)
    F = np.empty(len(xx))
    for i in range(len(xx)):
        F[i] = c[0]*funs[0](xx[i]) + c[1]*funs[1](xx[i]) + c[2]*funs[2](xx[i])

    plt.errorbar(x, y, dy, color='b', fmt='o', label="Data")
    plt.plot(xx, F, label="Fit", color='r')
    plt.xlabel("x")
    plt.ylabel("y")
    plt.title("Least squares fit")
    plt.legend()
    plt.savefig("plot.svg")

    print("\nSee plot for graphic visualization of fit.")

    print("\n>> Exercise B")
    print("The covariance matrix is")
    print("S = ", S)
    print("\nThe coefficients are therefore given by")
    print("c1 = " + str(c[0]) + " +/- " + str(sqrt(S[0, 0])))
    print("c2 = " + str(c[1]) + " +/- " + str(sqrt(S[1, 1])))
    print("c3 = " + str(c[2]) + " +/- " + str(sqrt(S[2, 2])))
