>>Exercise A
Fit the following data:
x =  [ 0.1   1.33  2.55  3.78  5.    6.22  7.45  8.68  9.9 ]
y =  [-15.3     0.32    2.45    2.75    2.27    1.35    0.157  -1.23   -2.75 ]
error_y =  [ 1.04   0.594  0.983  0.998  1.11   0.398  0.535  0.968  0.478]

Running least squares fit...
Done. Found the following coefficients:
c =  [ 6.99066524  0.99047253 -1.99641144]

See plot for graphic visualization of fit.

>> Exercise B
The covariance matrix is
S =  [[ 0.1777794   0.04934616 -0.05276833]
 [ 0.04934616  0.24817971 -0.04584551]
 [-0.05276833 -0.04584551  0.02081985]]

The coefficients are therefore given by
c1 = 6.99066524184 +/- 0.4216389422839372
c2 = 0.99047253104 +/- 0.49817638455703495
c3 = -1.99641143616 +/- 0.1442908663655714
