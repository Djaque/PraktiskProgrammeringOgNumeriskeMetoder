-- Question 1 --
For this exercise I am using the Monte Carlo part A code, which takes around 35s to compute (see out_A.txt in Monte Carlo directory.
The code is run 3 times, which would take over 1.5min. The time used for these 3 executions is printed at the end of this file.

 Calculating the integral of f(x) = sqrt(x) from  [ 0.]  to  [ 1.]  using plain Monte Carlo.
Result       =  0.666728010252
Expected     =  0.6666666666666666
Error        =  0.000166605303762
Time         =  24.987

 Calculating the integral of f(x) in 3 dimensions given in the exercise from  [ 0.  0.  0.]  to  [ 3.14159265  3.14159265  3.14159265]  using plain Monte Carlo.
Result       =  1.38947112299
Expected     =  1.3932039296856775
Error        =  0.00785613521098
Time         =  15.962

 Total time used = 40.948

 Calculating the integral of f(x) = sqrt(x) from  [ 0.]  to  [ 1.]  using plain Monte Carlo.
Result       =  0.666728010252
Expected     =  0.6666666666666666
Error        =  0.000166605303762
Time         =  25.610

 Calculating the integral of f(x) in 3 dimensions given in the exercise from  [ 0.  0.  0.]  to  [ 3.14159265  3.14159265  3.14159265]  using plain Monte Carlo.
Result       =  1.38947112299
Expected     =  1.3932039296856775
Error        =  0.00785613521098
Time         =  16.010

 Total time used = 41.620

 Calculating the integral of f(x) = sqrt(x) from  [ 0.]  to  [ 1.]  using plain Monte Carlo.
Result       =  0.666728010252
Expected     =  0.6666666666666666
Error        =  0.000166605303762
Time         =  26.055

 Calculating the integral of f(x) in 3 dimensions given in the exercise from  [ 0.  0.  0.]  to  [ 3.14159265  3.14159265  3.14159265]  using plain Monte Carlo.
Result       =  1.38947112299
Expected     =  1.3932039296856775
Error        =  0.00785613521098
Time         =  15.655

 Total time used = 41.710

Total time using multiprocessing was 41.722 s
