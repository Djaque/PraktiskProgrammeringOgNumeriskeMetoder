import sys
sys.path.append('../Monte_Carlo')
import mc_main as mc
import multiprocessing as mp
import time


if __name__ == '__main__':
    t = time.time()

    print("-- Question 1 --")
    print("For this exercise I am using the Monte Carlo part A code, which takes around 35s to compute (see out_A.txt in Monte Carlo directory.")
    print("The code is run 3 times, which would take over 1.5min. The time used for these 3 executions is printed at the end of this file.")
    p1 = mp.Process(target=mc.main_A)
    p2 = mp.Process(target=mc.main_A)
    p3 = mp.Process(target=mc.main_A)

    p1.start()
    p2.start()
    p3.start()

    p1.join()
    p2.join()
    p3.join()

    print("\nTotal time using multiprocessing was %.3f" % (time.time()-t), "s")


