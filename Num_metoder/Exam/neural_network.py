import numpy as np
import scipy.optimize as opt


class Network1D:
    def __init__(self, num_neurons, func, func_deriv):
        # Set attributes for the network
        self.n = num_neurons
        self.f = func
        self.df = func_deriv
        self.params = np.random.rand(self.n, 3)   # rows for a, b and w

    def feed_forward(self, input):
        output_val = 0
        output_deriv = 0
        x = input
        for i in range(self.n):
            # Get parameters a, b and w
            a = self.params[i][0]
            b = self.params[i][1]
            w = self.params[i][2]

            output_val += self.f((x-a)/b)*w
            output_deriv += self.df((x-a)/b)*w/b

        return output_val, output_deriv

    def train(self, input_data, ode_func, init_vals):
        N = input_data.size
        x = input_data
        x0 = init_vals[0]
        y0 = init_vals[1]

        def cost_function(p):   # Delta function (to be optimized)
            self.params = np.reshape(p, (self.n, 3))    # reshape to fit params
            y, dy = self.feed_forward(x)
            f = ode_func(x, y)
            cost = np.sum(np.abs(dy-f)**2) + N*np.abs(self.feed_forward(x0)[0]-y0)**2

            return cost

        # Minimize/optimize parameters
        opt.minimize(cost_function, np.reshape(self.params, [1, 3*self.n]), method='BFGS', tol=1e-6)
