import sys
import numpy as np
import neural_network as nn
from matplotlib import pyplot as plt


def logistic():
    def log_func(x):
        # Returns the logistic function value(s)
        val = np.divide(1, 1 + np.exp(-x))
        return val

    def log_func_deriv(x):
        # Returns the logistic function derivative value(s)
        return log_func(x) * (1 - log_func(x))

    def ode_log(x, y):
        # Returns the logistic ODE function
        return y * (1 - y)

    a = -5  # Starting point
    b = 5  # End point
    N = 60  # Number of points

    # Initial condition (x0, y0)
    log_init = np.array([0, 0.5])

    # Using numpy's linspace function instead of the equation in the exercise, as this does the same
    x = np.linspace(a, b, N)

    # Number of hidden neurons in the network
    num_neurons = 20

    # Initializing network
    nw = nn.Network1D(num_neurons, log_func, log_func_deriv)

    # Initial parameter guesses
    initial_params_guess = np.empty([num_neurons, 3])     # Initial parameters of a, b and w for all neurons
    for i in range(num_neurons):     # Loop over all the neurons 3 parameters
        initial_params_guess[i][0] = a + (b-a) * i  # parameter a (shift)
        initial_params_guess[i][1] = 1              # parameter b (scaling)
        initial_params_guess[i][2] = 1              # parameter w (weighting)
    nw.params = initial_params_guess

    # Training network
    nw.train(x, ode_log, log_init)

    ### Data print for exporting
    y = log_func(x)
    yy = nw.feed_forward(x)[0]
    for i in range(y.size):
        print(x[i], "\t", y[i], "\t", yy[i])


def gauss():
    def gauss_func(x):
        # Returns the Gauss function value(s)
        return np.exp(-x * x / 2)

    def gauss_func_deriv(x):
        val = -x * gauss_func(x)
        return val

    def ode_gauss(x, y):
        # Returns the Gauss ODE function
        return -x * y

    a = -5  # Starting point
    b = 5  # End point
    N = 60  # Number of points

    # Initial condition
    gauss_init = np.array([0, 1])

    # Using numpy's linspace function instead of the equation in the exercise, as this does the same
    x = np.linspace(a, b, N)

    num_neurons = 20  # Number of hidden neurons in the network

    nw = nn.Network1D(num_neurons, gauss_func, gauss_func_deriv)  # Initializing network

    # Initial parameter guesses
    initial_params_guess = np.empty([num_neurons, 3])  # Initial parameters of a, b and w for all neurons
    for i in range(num_neurons):  # Loop over all the neurons 3 parameters
        initial_params_guess[i][0] = a + (b - a) * i/(nw.n-1)  # parameter a (shift)
        initial_params_guess[i][1] = 1  # parameter b (scaling)
        initial_params_guess[i][2] = 1  # parameter w (weighting)
    nw.params = initial_params_guess

    nw.train(x, ode_gauss, gauss_init)  # Training network


    ### Data print for exporting
    y = gauss_func(x)
    yy = nw.feed_forward(x)[0]
    for i in range(y.size):
        print(x[i], "\t", y[i], "\t", yy[i])


def plot(data_file):
    # Data extraction
    data = np.loadtxt(data_file)
    x = np.empty(data.shape[0])
    y = np.empty(data.shape[0])
    yy = np.empty(data.shape[0])
    counter = 0
    for row in data:
        x[counter] = row[0]
        y[counter] = row[1]
        yy[counter] = row[2]
        counter += 1

    # Plotting
    plt.scatter(x, y, marker='o', label='Expected')
    plt.plot(x, yy, 'r', label='Neural net', linewidth=2)
    plt.xlabel("x")
    plt.ylabel("y")
    string = data_file[0:(-1 - 8)]  # Data file string for plot title and save-to-filename string
    plt.title("Neural net solution - " + string + " function")
    plt.legend()

    # Save figure
    save_string = "" + string + "_ode.svg"
    plt.savefig(save_string)


if __name__ == '__main__':
    if sys.argv[1:][0] == 'log':
        logistic()
    elif sys.argv[1:][0] == 'gauss':
        gauss()
    elif sys.argv[1:][0] == 'plot':
        plot(sys.argv[1:][1])
    else:
        for arg in sys.argv[1:]:
            print("Please provide the correct assignment request. Provided: ", arg)


