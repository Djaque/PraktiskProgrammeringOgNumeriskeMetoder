import numpy as np
import scipy.optimize as opt


class Network1D:
    def __init__(self, num_neurons, func):
        self.n = num_neurons
        self.f = func
        self.params = np.random.rand(self.n, 3)   # rows for a, b and w

    def feed_forward(self, input):
        output = 0
        x = input
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            w = self.params[i][2]

            output += self.f((x-a)/b)*w

        return output

    def train(self, input_data, labels):
        assert input_data.size == labels.size

        def cost_function(p):
            self.params = np.reshape(p, (self.n, 3))    # reshape to fit params
            f = labels
            y = self.feed_forward(input_data)
            cost = np.sum((y-f)**2)

            return cost

        opt.minimize(cost_function, np.random.rand(3*self.n), method='BFGS', tol=1e-6)


class Network2D:
    def __init__(self, num_neurons, func):
        self.n = num_neurons
        self.f = func
        self.params = np.random.rand(self.n, 5)   # rows for a, b, c, d and w

    def feed_forward(self, input):
        output = 0
        x = input[0]
        y = input[1]
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            c = self.params[i][2]
            d = self.params[i][3]
            w = self.params[i][4]

            output += self.f((x-a)/b, (y-c)/d)*w

        return output

    def train(self, input_data, labels):
        # assert input_data.size == labels.size

        def cost_function(p):
            self.params = np.reshape(p, (self.n, 5))    # reshape to fit params
            f = labels
            z = self.feed_forward(input_data)
            cost = np.sum((z-f)**2)

            return cost

        opt.minimize(cost_function, np.random.rand(5*self.n), method='BFGS', tol=1e-6)
