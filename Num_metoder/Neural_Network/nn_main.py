import neural_network as nn
import numpy as np
import sys
from matplotlib import pyplot as plt
from mpl_toolkits import mplot3d


def f(x, y=None):
    if y is None:
        return x * np.exp(-x * x)
    else:
        return y * x * np.exp(-x * x) * np.exp(-y * y)


def g(x, y=None):
    if y is None:
        return np.cos(5*x)*np.exp(-x*x)
    else:
        return np.cos(5 * x) * np.exp(-x * x) * np.exp(-y * y)


def main_A():
    size = 20
    nw = nn.Network1D(size, f)
    x = np.linspace(-1.5, 1.5, 100)
    labels = g(x)
    nw.train(x, labels)
    y = nw.feed_forward(x)

    plt.scatter(x, labels, label="Target function")
    plt.plot(x, y, color='red', label="Neural net")
    plt.xlim([-1.5, 1.5])
    plt.title("Artificial Neural Networks")
    plt.xlabel("x")
    plt.ylabel("f(x)")
    plt.legend()
    plt.savefig("plot_A.svg")


def main_B():
    size = 20
    nw = nn.Network2D(size, f)
    x = np.linspace(-1.5, 1.5, 100)
    y = np.linspace(-2.5, 2.5, 100)
    labels = g(x, y)
    nw.train([x, y], labels)
    z = nw.feed_forward([x, y])

    plt.figure()
    ax = plt.axes(projection='3d')
    ax.plot3D(x, y, z, color='r', label="Target function")
    ax.scatter3D(x, y, labels, label="Target function")

    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_title('Neural network in 2 dimensions')
    plt.savefig("plot_B.svg")


if __name__ == '__main__':
    if sys.argv[1:][0] == 'A':
        main_A()
    elif sys.argv[1:][0] == 'B':
        main_B()
    else:
        for arg in sys.argv[1:]:
            print("Please provide the correct assignment request. Provided: ", arg)
