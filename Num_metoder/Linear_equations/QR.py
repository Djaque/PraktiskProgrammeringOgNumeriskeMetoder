import numpy as np


def qr_decomp(A):
    n = A.shape[0]
    m = A.shape[1]

    Q = A.copy()
    R = np.zeros((m, m))

    for i in range(m):
        R[i, i] = np.linalg.norm(Q[:, i])
        Q[:, i] /= R[i, i]
        for j in range(i+1, m):
            R[i, j] = np.dot(Q[:, i], Q[:, j])
            Q[:, j] -= R[i, j] * Q[:, i]

    assert Q.shape == A.shape   #Make sure Q is (n x m) matrix
    assert R.shape[0] == R.shape[1] and R.shape[0] == m    #Make sure R is a square (m x m) matrix

    return Q, R


def qr_solve(Q, R, b):
    c = np.dot(np.transpose(Q), b)
    n = len(c)
    x = np.zeros(n)

    for i in reversed(range(n)):
        c_i = c[i]
        for j in range(i+1, n):
            c_i -= R[i, j] * x[j]

        x[i] = c_i / R[i, i]

    return x


def qr_inverse(Q, R, eps=1e-12):
    n = np.matmul(Q,R).shape[0]
    B = np.eye(n)
    A_inverse = np.empty((n, n))
    i = 0
    for column in B:
        A_inverse[:, i] = qr_solve(Q, R, column)
        i += 1

    return A_inverse
