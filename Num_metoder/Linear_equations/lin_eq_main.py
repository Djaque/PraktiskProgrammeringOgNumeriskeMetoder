import numpy as np
import QR


def approx_equal(A, B, epsilon=1e-10):
    assert A.shape == B.shape
    return (abs(A-B) < epsilon).all()


def lin_eq_solve(A, b):
    Q, R = QR.qr_decomp(A)
    x = QR.qr_solve(Q, R, b)

    return x


def partA_1():
    print("> Part 1")
    n = 6
    m = 3
    assert n > m
    A = np.random.rand(n, m) * 10

    print("\nNow running QR decomposition on matrix A(size=" + str(n) + "x" + str(m) + ")...")
    Q, R = QR.qr_decomp(A)
    print("Done.")

    print("Q = ")
    print(Q)

    print("R = ")
    print(R)

    print("\nChecking that R is upper triangular...")
    is_triangular = True
    for j in range(m):
        for i in range(j + 1, m):
            if R[i, j] != 0:
                is_triangular = False
                break

    assert is_triangular
    print("Success! R is upper triangular.")

    print("\nChecking that Q^T*Q is 1...")
    if (abs(np.matmul(np.transpose(Q), Q))-np.eye(m) < 1e-12).all():
        print("Success!")
        print(np.matmul(np.transpose(Q), Q))
    else:
        print("Failure, Q^T*Q=")
        print(np.matmul(np.transpose(Q), Q))

    print("\nChecking that QR=A...")
    assert approx_equal(A, np.matmul(Q, R))
    print("Success!")


def partA_2():
    print("\n> Part 2")
    n = 5
    A = np.random.rand(n, n) * 10
    b = np.random.rand(n) * 5

    print("Square matrix A:")
    print(A)
    print("\nVector b:")
    print(b)

    print("\nSolving Ax = b for x...")
    Q, R = QR.qr_decomp(A)
    x = QR.qr_solve(Q, R, b)
    print("Done! x = ")
    print(x)

    assert (np.dot(A, x) - b < 1e-12).all()
    print("\nAx = ")
    print(np.dot(A, x))


def partB():
    n = 5
    A = np.random.rand(n, n) * 10

    Q, R = QR.qr_decomp(A)
    A_inv = QR.qr_inverse(Q, R)
    print("A is")
    print(A)
    print("\nThe inverse of A is")
    print(A_inv)

    print("\nThe product of the A and A_inv is")
    print(np.matmul(A, A_inv))
    print("which is the identity matrix within precision epsilon.")


if __name__ == '__main__':

    print("- - - Exercise A - - - ")
    partA_1()
    partA_2()

    print("\n\n- - - Exercise B - - - ")
    partB()
