import newton
from math import *
import numpy as np


def f1(x, A=10000):
    f = np.empty(2)
    f[0] = A*x[0]*x[1]-1
    f[1] = exp(-x[0])+exp(-x[1])-1/A-1

    return f


def rosenbrock(x):
    f = np.empty(2)
    f[0] = -2*(1-x[0])-1100*2*(x[1]-x[0]**2)*2*x[0]
    f[1] = 100*2*(x[1]-x[0]**2)

    return f


def himmelblau(x):
    f = np.empty(2)
    f[0] = 2*(2*x[0] * (x[0]**2 + x[1] - 11) + x[0] + x[1]**2 - 7)
    f[1] = 2*(x[0]**2 + 2*x[1]*(x[0] + x[1]**2 - 7) + x[1] - 11)

    return f


def fA(p):
    z = np.empty(2);
    x = p[0]
    y = p[1]
    A = 10000
    z[0] = A*x*y - 1
    z[1] = np.exp(-x) + np.exp(-y) - 1 - 1/A

    return z


def dfA(p):
    J = np.empty((2, 2))
    x = p[0]
    y = p[1]
    A = 10000
    J[0, 0] = A*y
    J[0, 1] = A*x
    J[1, 0] = np.exp(-x)*(-1)
    J[1, 1] = np.exp(-y)*(-1)

    return J


if __name__ == '__main__':
    print(">> Exercise A")

    print("\nSystem of equations")
    x_start = np.array([1, 5])
    print("Starting points = ", x_start)
    solution = newton.newton(f1, x_start, 1e-3)
    print("Found solution = ", solution)

    print("\nRosenbrock function:")
    x_start = np.array([5, 10])
    print("Starting points = ", x_start)
    solution = newton.newton(rosenbrock, x_start, 1e-6)
    print("Solution found = ", solution)

    print("\nHimmelblau function:")
    x_start = np.array([5, 10])
    print("Starting points = ", x_start)
    solution = newton.newton(himmelblau, x_start, 1e-6)
    print("Solution found = ", solution)


    print("\n\n>> Exercise B")
    solution = newton.newton_with_jac(fA, x_start, dfA)
    print(solution)
