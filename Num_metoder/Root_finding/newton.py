import sys
sys.path.append('../Linear_equations')
import lin_eq_main
import numpy as np


def newton(fun, x_start, dx, epsilon=1e-6):
    x = x_start.copy().astype(float)  # (x1,x2) or (x,y), convert to float in case of integers
    n = len(x_start)
    J = np.empty([n, n])

    while np.linalg.norm(fun(x)) > epsilon:
        fx = fun(x)
        for j in range(n):
            x[j] += dx
            df = fun(x)-fx
            for i in range(n):
                J[i, j] = df[i]/dx
            x[j] -= dx

        delta_x = lin_eq_main.lin_eq_solve(J, -fun(x))
        l = 1   #Lambda
        while np.linalg.norm(fun(x)+l*delta_x) > (1-l/2)*np.linalg.norm(fun(x)) and (l > 1/64):
            l = l/2
            x = x + l*delta_x

    return x


def newton_with_jac(fun, x_start, J, epsilon=1e-6):
    x = x_start.copy().astype(float)  # (x1,x2) or (x,y), convert to float in case of integers
    n = len(x_start)

    while np.linalg.norm(fun(x)) > epsilon:
        delta_x = lin_eq_main.lin_eq_solve(J, -fun(x))
        l = 1  # Lambda
        while np.linalg.norm(fun(x) + l * delta_x) > (1 - l / 2) * np.linalg.norm(fun(x)) and (l > 1 / 64):
            l = l / 2
            x = x + l * delta_x

    return x
