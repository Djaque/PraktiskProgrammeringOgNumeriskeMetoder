from math import *


def lin_interp(x, y, z):
    assert len(x) == len(y)

    i = 0
    j = len(x)-1
    while j-i > 1:
        m = floor((i+j)/2)
        if z > x[m]:
            i = m
        else:
            j = m

    return y[i] + (z-x[i]) * (y[i+1]-y[i]) / (x[i+1]-x[i])


def lin_integral(x, y, z):
    value = 0
    for i in range(len(x)-1):
        if x[i+1] > z:
            y_val = lin_interp(x, y, z)
            delta_x = z - x[i]
            area = abs(y[i]+y_val)/2 * delta_x
            assert area > 0
            value += area
            break
        else:
            delta_x = x[i+1]-x[i]
            area = abs(y[i]+y[i+1])/2 * delta_x
            assert area > 0
            value += area

    return value
