from math import *
import numpy as np


def qspline(x, y):
    n = len(x)
    h = np.empty(n-1)
    p = np.empty(n-1)
    a = np.zeros(n-1)

    for i in range(n-1):
        h[i] = x[i+1]-x[i]
        p[i] = (y[i+1]-y[i])/h[i]

    for i in range(n-2):
        a[i+1] = (p[i+1]-p[i]-a[i]*h[i])/h[i+1]

    a[-1] /= 2
    for i in reversed(range(n-2)):
        a[i] = (p[i+1]-p[i]-a[i+1]*h[i+1])/h[i]

    def eval(z):
        i = 0
        j = n-1
        while j-i > 1:
            l = floor((i+j)/2)
            if z >= l:
                i = l
            else:
                j = l
        return y[i]+(p[i]+a[i]*(z-x[i+1]))*(z-x[i])
    spline = np.empty(n)

    # for i in range(n):
    #     spline[i] = eval(x[i])

    return eval
