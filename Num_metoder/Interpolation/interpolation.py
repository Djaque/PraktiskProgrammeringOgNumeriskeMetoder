from lin_interp import *
from quad_interp import *
import random
from math import *
from matplotlib import pyplot as plt
import numpy as np
import sys


def main_A(x, y, num_points):
    N = len(x)
    n = num_points

    step = (x[-1] - x[0]) / n
    print("Exercise A - Part 1:\n")
    print("#Random points (x,y)")

    for i in range(len(x)):
        print("%.3f \t %.3f" % (x[i], y[i]))
    print("\nInterpolated points")
    z = x[0]
    x_interp = []
    y_interp = []
    z_interp = []
    while z <= x[-1]:
        print("%.3f \t %.3f" % (z, lin_interp(x, y, z)))
        x_interp.append(z)
        y_interp.append(lin_interp(x, y, z))
        z += step
        z_interp.append(z)
    z = 4 + 0.5 * random.random()

    print("\nExercise A - Part 2:\n")
    print("Integral from %.3f to %.3f:" % (x[0], z))
    print("Answer = %.3f" % lin_integral(x, y, z))
    print("Expected (by numpy.trapz() calculation) = %.3f" % np.trapz(y))
    print("\nExercise A - Part 3:\n")
    print("See plot")
    integ = []
    for i in z_interp:
        integ.append(lin_integral(x, y, i))

    return x_interp, y_interp


def main_B(x, y, num_points):
    N = len(x)
    n = num_points
    spline = qspline(x, y)
    print("z \t spline")
    x_interp = np.linspace(x[0], x[-1], n)
    y_interp = np.empty(n)
    for i in range(n):
        y_interp[i] = spline(x_interp[i])
        print("%.2f \t %.2f" % (x_interp[i], y_interp[i]))

    return x_interp, y_interp



if __name__ == '__main__':
    N = 10
    num_points = 100
    x = [i + 0.5 * random.random() for i in range(N)]
    y = [sin(0.6 * t) for t in x]

    if sys.argv[1:][0] == 'A':
        main_A(x, y, num_points)
    elif sys.argv[1:][0] == 'B':
        main_B(x, y, num_points)
    elif sys.argv[1:][0] == 'plot':
        x_lin, y_lin = main_A(x, y, num_points)
        x_quad, y_quad = main_B(x, y, num_points)
        plt.plot(x_lin, y_lin, label="Linear interp")
        plt.plot(x_quad, y_quad, label="Quadratic interp")
        plt.scatter(x, y, color='Red', label="Points")

        plt.title("Interpolation exercise")
        plt.xlabel("x")
        plt.ylabel("y")
        plt.xlim([-1, 10])
        plt.legend()
        plt.savefig("plot.svg")
    else:
        for arg in sys.argv[1:]:
            print("Please provide the correct assignment request. Provided: ", arg)

