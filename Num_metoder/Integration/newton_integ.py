from math import *


def closed(func, a, b, acc=1e-3, eps=1e-3):
    func1 = func(a)
    func3 = func(b)
    return closed_reuse_points(func, a, b, acc, eps, func1, func3)


def closed_reuse_points(func, a, b, acc, eps, func1, func3):
    func2 = func(a + (b-a)/2)
    Q = (func1 + 4*func2 + func3)/6 * (b-a)
    q = (func1 + 2*func2 + func3)/4 * (b-a)

    err = abs(Q-q)/2
    tol = acc + abs(Q)*eps

    if err < tol:
        return Q
    else:
        new_point = (a + b)/2
        Q1 = closed_reuse_points(func, a, new_point, acc/sqrt(2), eps, func1, func2)
        Q2 = closed_reuse_points(func, new_point, b, acc/sqrt(2), eps, func2, func3)
        return Q1 + Q2
