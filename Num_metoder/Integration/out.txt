-- Question 1 -- 
A) Implement recursive adaptive integrator.

 Evaluating the integral of f(x) = sqrt(x) from  0  to  1 .
Result       =  0.6666559338797313
Expected     =  0.6666666666666666
# of calls   =  27

 Evaluating the integral of f(x) = 4*sqrt(1-(1-x))*(1-x) from  0  to  1 .
Result       =  3.141592647979502
Expected     =  3.141592653589793
# of calls   =  345
