import math
import sys
import newton_integ


def f1(x):
    global ncalls
    ncalls += 1
    return math.sqrt(x)


def f2(x):
    global ncalls
    ncalls += 1
    return 4*math.sqrt(1-(1-x)*(1-x))


ncalls=0
sys.setrecursionlimit(15000)


a = 0
b = 1
acc = 1e-4
eps = 1e-4

Q = newton_integ.closed(f1, a, b, acc, eps)
expected = 2/3
print("-- Question 1 -- ")
print("A) Implement recursive adaptive integrator.")

print("\n Evaluating the integral of f(x) = sqrt(x) from ", a, " to ", b, ".")
print("Result       = ", Q)
print("Expected     = ", expected)
print("# of calls   = ", ncalls)


acc = 1e-6
eps = 1e-6
ncalls = 0
Q = newton_integ.closed(f2, a, b, acc, eps)
expected = math.pi

print("\n Evaluating the integral of f(x) = 4*sqrt(1-(1-x))*(1-x) from ", a, " to ", b, ".")
print("Result       = ", Q)
print("Expected     = ", expected)
print("# of calls   = ", ncalls)
