from math import *


def approx_equal(a, b, epsilon=float(1e-6)):
    if abs(a-b) < epsilon:
        return True
    elif abs(a-b)/(abs(abs(a))+abs(b)) < epsilon:
        return True
    else:
        return False


def jac_rotation(A):
    assert A.shape[0] == A.shape[1]
    N = A.shape[0]
    converged = False
    sweeps = 0
    while not converged:
        converged = True
        sweeps += 1
        for column in reversed(range(N)):
            for row in range(column):
                p = row
                q = column
                phi = 0.5*atan2(2*A[p][q], A[q][q]-A[p][p])
                s = sin(phi)
                c = cos(phi)

                a_pq = A[p][q]
                a_pp = A[p][p]
                a_qq = A[q][q]

                a_pp_new = c**2 * a_pp - 2 * s * c * a_pq + s**2 * a_qq
                a_qq_new = s**2 * a_pp + 2 * s * c * a_pq + c**2 * a_qq

                if a_pp_new != a_pp or a_qq_new != a_qq:
                    if approx_equal(a_pp_new, a_pp) or approx_equal(a_qq_new, a_qq) is False:
                        converged = False
                        A[p][p] = a_pp_new
                        A[q][q] = a_qq_new

                        for i in range(p):
                            a_ip = A[i][p]
                            a_iq = A[i][q]
                            A[i][p] = c * a_ip - s * a_iq
                            A[i][q] = s * a_ip + c * a_iq

                        for i in range(p+1, q):
                            a_pi = A[p][i]
                            a_iq = A[i][q]
                            A[p][i] = c * a_pi - s * a_iq
                            A[i][q] = s * a_pi + c * a_iq

                        for i in range(q+1, N):
                            a_pi = A[p][i]
                            a_qi = A[q][i]
                            A[p][i] = c * a_pi - s * a_qi
                            A[q][i] = s * a_pi + c * a_qi
                A[p][q] = 0
                A[q][p] = 0
    return sweeps