import numpy as np
import time
from eigen import sym_matrix
from jacobi import jac_rotation
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit


def power_func(x, a, c):
    return a*np.power(x, c)

def timing():
    N = range(3, 30)

    print("Calculating time dependence for "+ str(len(N)) + " datapoints...")


    times = np.zeros(len(N))
    tid = time.time()
    counter = 0
    for n in N:
        A = sym_matrix(n)
        jac_rotation(A)
        times[counter] = time.time()-tid
        counter += 1

    z, zz = curve_fit(power_func, N, times)
    function_string = "fit=O(n^" + str(np.around(z[1], 2)) + ")"

    plt.scatter(N, times, label='Algorithm', marker='.')
    plt.plot(N, power_func(N, z[0], z[1]), label=function_string)
    plt.xlabel("Matrix size")
    plt.ylabel("Time [s]")
    plt.legend()

    plt.savefig("partA_timing.svg")

    print(" ")
    print("Done. See image file for result.")

    print(" ")
    print("Note: Running time ranges between O(n^3.5) to O(n^4.1), but I cannot find a reason for the algorithm to be "
          "larger than O(n^3), since there are only 3 nested for-loops which is equal to your (Dimitri) own code.")

if __name__ == '__main__':
    timing()