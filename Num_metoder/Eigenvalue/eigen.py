import numpy as np
from jacobi import *


def sym_matrix(size):
    N = size
    b = np.random.random_integers(-200, 200, size=(N, N))
    A = (b + b.T) / 2
    return A


if __name__ == '__main__':
    print(">> Eigenvalues assignment<< ")
    print("- Part A")
    print(" ")

    N = 5
    A = sym_matrix(N)
    eig = np.sort(np.linalg.eigvals(A))

    print("Matrix A(size=" + str(N) + ") = ")
    print(A)
    print(" ")

    sweeps = jac_rotation(A)


    print(" ")
    print("Diagonal matrix = ")
    print(A)

    print("")
    print("Sweeps required: " + str(sweeps))
    print(" ")
    print("Expected eigenvalues (from numpy): ")
    print(eig)
    print("Calculated eigenvalues with cyclic sweeps:")
    print(np.sort(np.diag(A)))
