import sys
sys.path.append('../Linear_equations')
import lin_eq_main as lin_solver
import numpy as np


def newton(func, point, eps=1e-7,):
    x = point.copy().astype(float)  # (x1,x2) or (x,y), convert to float in case of integers
    alpha = 1e-2
    steps = 0
    while True:
        steps += 1
        fx, H, dfdx = func(x)  # Get func value, Hessian matrix and gradient
        delta_x = lin_solver.lin_eq_solve(H, -dfdx)
        l = 1  # Lambda
        while True:
            s = l * delta_x
            y = x + s
            fy, H, dfy = func(y)
            if fy < fx + alpha * np.dot(np.transpose(s), dfdx) or l < 1 / 64:
                break
            l = l / 2
        x = y
        dfdx = dfy
        if np.linalg.norm(dfdx) < eps:
            break

    return x, steps


def quasi_newton(func, point, eps=1e-7, dx=1e-12):
    def gradient(f, x, dx):
        fx = f(x)[0]
        dfdx = np.empty(x.size)

        for i in range(x.size):
            x[i] += dx
            dfdx[i] = (f(x)[0] - fx) / dx
            x[i] -= dx

        return dfdx

    x = point.copy().astype(float)
    dfdx = gradient(func, x, dx)
    fx = func(x)[0]
    B = np.eye(x.size, dtype='float64')
    alpha = 1e-2
    steps = 0

    while True:
        steps += 1
        Dx = np.dot(B, -dfdx)
        l = 2   #lambda

        while True:  # linesearch
            l /= 2
            s = Dx * l  # s = Dx * lambda
            z = x + s
            fz = func(z)[0]
            if abs(fz) < abs(fx) + alpha * np.dot(s, dfdx):  # Armijo condition
                break  # ok
            if np.linalg.norm(s) < dx:
                B = np.eye(x.size, dtype='float64')  # restart
                break

        dfdx_z = gradient(func, z, dx)
        y = dfdx_z - dfdx
        u = s - np.dot(B, y)

        # symmetric Broyden's update
        if abs(np.dot(y, s)) > eps:
            gamma = np.dot(u, y) / (2*np.dot(s, y))
            a = (u - gamma * s) / np.dot(s, y)
            B += np.outer(s, a) + np.outer(a, s)

        x = z
        fx = fz
        dfdx = dfdx_z
        if np.linalg.norm(dfdx) < eps or np.linalg.norm(Dx) < dx:
            break

    return x, steps