import numpy as np
import minimizer as min
from matplotlib import pyplot as plt


def rosenbrock(point):
    a = 1; b = 100
    x = point[0]
    y = point[1]
    z = (a-x)**2 + b*(y-x**2)**2

    n = point.size
    H = np.empty([n, n])
    H[0, 0] = 400*(3*x**2-y)+2
    H[0, 1] = -400*x
    H[1, 0] = H[0, 1]
    H[1, 1] = 200

    dfdx = np.empty(2)
    dfdx[0] = 2*(200*x**3-200*x*y+x-1)
    dfdx[1] = 100 * 2 * (y - x ** 2)

    return z, H, dfdx


def himmelblau(point):
    a = 7; b = 11
    x = point[0]
    y = point[1]
    z = (x**2+y-b)**2 + (x+y**2-a)**2

    n = point.size
    H = np.empty([n, n])
    H[0, 0] = 12*x**2 + 4*y - 42
    H[0, 1] = 4*(x+y)
    H[1, 0] = H[0, 1]
    H[1, 1] = 4*x + 12*y**2 - 26

    dfdx = np.empty(2)
    dfdx[0] = 2 * (2 * x * (x ** 2 + y - 11) + x + y ** 2 - 7)
    dfdx[1] = 2 * (x ** 2 + 2 * y * (x + y ** 2 - 7) + y - 11)

    return z, H, dfdx


def decay(t, p):
    A = p[0]
    T = p[1]
    B = p[2]

    return A*np.exp(-t/T)+B


def master_func(t, y, s, p):
    summ = 0
    for i in range(t.size):
        summ += ((decay(t[i], p)-y[i])**2)/(s[i]**2)

    return np.array([summ])


if __name__ == '__main__':
    print("-- Question 1 --")
    print(" A) Implement newton minimization and find minimum of Rosenbrock and Himmelblau.")
    point = np.array([18.0, 15.0])
    q, steps = min.newton(rosenbrock, point)
    print("\nRosenbrock minimum at ", q)
    print("Number of steps required ", steps)

    q, steps = min.newton(himmelblau, point)
    print("\nHimmelblau minimum at ", q)
    print("Number of steps required ", steps)



    print("\n\n-- Question 2 --")
    print(" A) Implement quasi newton minimization and find minimum of Rosenbrock and Himmelblau.")
    point = np.array([3., 3.])
    q, steps = min.quasi_newton(rosenbrock, point)
    print("\nRosenbrock minimum at ", q)
    print("Number of steps required ", steps)

    q, steps = min.quasi_newton(himmelblau, point)
    print("\nHimmelblau minimum at ", q)
    print("Number of steps required ", steps)

    print("\n B) Compare methods")
    print("By comparing the stepsize, it seems the quasi newton method is the faster of the two.")

    print("\n C) Apply quasi newton method on given problem.")
    t = np.array([0.23, 1.29, 2.35, 3.41, 4.47, 5.53, 6.59, 7.65, 8.71, 9.77])
    y = np.array([4.64, 3.38, 3.01, 2.55, 2.29, 1.67, 1.59, 1.69, 1.38, 1.46])
    e = np.array([0.42, 0.37, 0.34, 0.31, 0.29, 0.27, 0.26, 0.25, 0.24, 0.24])

    func = lambda p: master_func(t, y, e, p)    # anonymous function to be minimized
    q, steps = min.quasi_newton(func, np.array([1, 1, 1]))

    # Plotting
    plt.errorbar(t, y, e, fmt='o', label='Data')
    x = np.linspace(0.1, 10, 80)
    activity = decay(x, q)
    plt.plot(x, activity, label='Fit')
    plt.xlabel("Time")
    plt.ylabel("Activity")
    plt.title("Test of quasi newton minimization on experimental data")
    plt.legend()
    plt.savefig("plot.svg")

    print("See plot for result")
